﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshGenerator : MonoBehaviour {

	MeshFilter meshFilter;
	Mesh mesh;
	List<Vector3> vertices;
	List<int> triangles;
	List<Vector3> normals;

	enum Facing { POS_X, NEG_X, POS_Y, NEG_Y, POS_Z, NEG_Z };

	float blockSize = 1f;

	// Use this for initialization
	void Start () {
		Debug.Log ("Mesh Generator");
		meshFilter = gameObject.GetComponent<MeshFilter>();
		mesh = new Mesh();

		vertices = new List<Vector3>();
		triangles = new List<int>();
		normals = new List<Vector3>();



		MakeCube(new Vector3(0,0,0), blockSize);
		MakeCube(new Vector3(0,blockSize,0), blockSize);
		MakeCube(new Vector3(0,blockSize*2,0), blockSize);

		MakeUpTrapeze(new Vector3(0,blockSize*3,0), 0.5f);

		MakePillar(new Vector3(blockSize,0,0), blockSize/4, 3f);
		MakeUpPillar(new Vector3(blockSize+blockSize/4,0,0), blockSize/4, 3f);
		MakeDownPillar(new Vector3(blockSize+blockSize/2,0,0), blockSize/4, 3f);


		UpdateMesh();

	}

	void MakeQuad(Vector3 pos, float size, Facing orientation){
		int insertedVertsIndex = vertices.Count;

		switch(orientation){

		case Facing.NEG_Z:
		{
			vertices.Add(new Vector3( pos.x, pos.y, pos.z ));
			vertices.Add(new Vector3( pos.x, pos.y+size, pos.z ));
			vertices.Add(new Vector3( pos.x+size, pos.y+size, pos.z ));
			vertices.Add(new Vector3( pos.x+size, pos.y, pos.z ));

			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+1);
			triangles.Add(insertedVertsIndex+2);

			triangles.Add(insertedVertsIndex+2);
			triangles.Add(insertedVertsIndex+3);
			triangles.Add(insertedVertsIndex);

			for(int i = 0; i < 4; i++){
				normals.Add(new Vector3( 0, 0, -1));
			}
			break;
		}

		case Facing.POS_Z:
		{
			vertices.Add(new Vector3( pos.x+size, pos.y, pos.z ));
			vertices.Add(new Vector3( pos.x+size, pos.y+size, pos.z ));
			vertices.Add(new Vector3( pos.x, pos.y+size, pos.z ));
			vertices.Add(new Vector3( pos.x, pos.y, pos.z ));
			
			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+1);
			triangles.Add(insertedVertsIndex+2);
			
			triangles.Add(insertedVertsIndex+2);
			triangles.Add(insertedVertsIndex+3);
			triangles.Add(insertedVertsIndex);
			
			for(int i = 0; i < 4; i++){
				normals.Add(new Vector3( 0, 0, 1));
			}
			break;
		}

		case Facing.POS_X:
		{
			vertices.Add(new Vector3( pos.x, pos.y, pos.z ));
			vertices.Add(new Vector3( pos.x, pos.y+size, pos.z ));
			vertices.Add(new Vector3( pos.x, pos.y+size, pos.z+blockSize ));
			vertices.Add(new Vector3( pos.x, pos.y, pos.z+blockSize ));
			
			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+1);
			triangles.Add(insertedVertsIndex+2);
			
			triangles.Add(insertedVertsIndex+2);
			triangles.Add(insertedVertsIndex+3);
			triangles.Add(insertedVertsIndex);
			
			for(int i = 0; i < 4; i++){
				normals.Add(new Vector3( 1, 0, 0));
			}
			break;
		}

		case Facing.NEG_X:
		{
			vertices.Add(new Vector3( pos.x, pos.y, pos.z ));
			vertices.Add(new Vector3( pos.x, pos.y, pos.z+blockSize ));
			vertices.Add(new Vector3( pos.x, pos.y+size, pos.z+blockSize ));
			vertices.Add(new Vector3( pos.x, pos.y+size, pos.z ));
			
			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+1);
			triangles.Add(insertedVertsIndex+2);
			
			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+2);
			triangles.Add(insertedVertsIndex+3);
			
			for(int i = 0; i < 4; i++){
				normals.Add(new Vector3( -1, 0, 0));
			}
			break;
		}

		case Facing.POS_Y:
		{
			vertices.Add(new Vector3( pos.x, pos.y, pos.z ));
			vertices.Add(new Vector3( pos.x, pos.y, pos.z+blockSize ));
			vertices.Add(new Vector3( pos.x+size, pos.y, pos.z+blockSize ));
			vertices.Add(new Vector3( pos.x+size, pos.y, pos.z ));
			
			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+1);
			triangles.Add(insertedVertsIndex+2);
			
			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+2);
			triangles.Add(insertedVertsIndex+3);
			
			for(int i = 0; i < 4; i++){
				normals.Add(new Vector3( 0, 1, 0));
			}
			break;
		}

		case Facing.NEG_Y:
		{
			vertices.Add(new Vector3( pos.x, pos.y, pos.z ));
			vertices.Add(new Vector3( pos.x+size, pos.y, pos.z ));
			vertices.Add(new Vector3( pos.x+size, pos.y, pos.z+blockSize ));
			vertices.Add(new Vector3( pos.x, pos.y, pos.z+blockSize ));
			
			triangles.Add(insertedVertsIndex);
			triangles.Add(insertedVertsIndex+1);
			triangles.Add(insertedVertsIndex+2);
			
			triangles.Add(insertedVertsIndex+2);
			triangles.Add(insertedVertsIndex+3);
			triangles.Add(insertedVertsIndex);
			
			for(int i = 0; i < 4; i++){
				normals.Add(new Vector3( 0, -1, 0));
			}
			break;
		}

		}
	}

	void MakeCube(Vector3 pos, float size){
		MakeQuad(new Vector3(pos.x+size, pos.y, pos.z), size, Facing.POS_X);
		MakeQuad(new Vector3(pos.x, pos.y, pos.z), size, Facing.NEG_X);


		MakeQuad(new Vector3(pos.x, pos.y+size, pos.z), size, Facing.POS_Y);
		MakeQuad(new Vector3(pos.x, pos.y, pos.z), size, Facing.NEG_Y);

		MakeQuad(new Vector3(pos.x, pos.y, pos.z+size), size, Facing.POS_Z);
		MakeQuad(new Vector3(pos.x, pos.y, pos.z), size, Facing.NEG_Z);

	}

	void MakeUpTrapeze(Vector3 pos, float size){
		MakeQuad(new Vector3(pos.x, pos.y, pos.z), size, Facing.NEG_Y);
		MakeQuad(new Vector3(pos.x+size, pos.y, pos.z), size, Facing.POS_X);

		MakeUpTriangle(new Vector3(pos.x, pos.y, pos.z), size);
		int firstTriangleLow = vertices.Count-3;
		int firstTriangleHigh = vertices.Count-2;

		MakeUpTriangle(new Vector3(pos.x, pos.y, pos.z+blockSize), size);
		int secTriangleLow = vertices.Count-3;
		int secTriangleHigh = vertices.Count-2;

		vertices.Add(vertices[firstTriangleLow]);
		vertices.Add(vertices[firstTriangleHigh]);
		vertices.Add(vertices[secTriangleLow]);
		vertices.Add(vertices[secTriangleHigh]);

		for(int i = 0; i < 4; i++){
			normals.Add(new Vector3(-1,1,0));
		}

		triangles.Add(vertices.Count-4);
		triangles.Add(vertices.Count-2);
		triangles.Add(vertices.Count-3);

		triangles.Add(vertices.Count-2);
		triangles.Add(vertices.Count-1);
		triangles.Add(vertices.Count-3);
	}

	void MakeUpTriangle(Vector3 pos, float size){
		int insertedVertsIndex = vertices.Count;

		vertices.Add( new Vector3(pos.x, pos.y, pos.z));
     	vertices.Add( new Vector3(pos.x + size, pos.y + size, pos.z));
		vertices.Add( new Vector3(pos.x + size, pos.y, pos.z));

		triangles.Add( insertedVertsIndex );
		triangles.Add( insertedVertsIndex+1);
		triangles.Add( insertedVertsIndex+2);

		for(int i = 0; i < 3; i++){
			normals.Add(new Vector3( 0, 0, -1));
		}
	}

	void MakeDownTrapeze(Vector3 pos, float size){
		MakeQuad(new Vector3(pos.x, pos.y, pos.z), size, Facing.NEG_Y);
		MakeQuad(new Vector3(pos.x, pos.y, pos.z), size, Facing.NEG_X);
		
		MakeDownTriangle(new Vector3(pos.x, pos.y, pos.z), size);
		int firstTriangleLow = vertices.Count-1;
		int firstTriangleHigh = vertices.Count-2;
		
		MakeDownTriangle(new Vector3(pos.x, pos.y, pos.z+blockSize), size);
		int secTriangleLow = vertices.Count-1;
		int secTriangleHigh = vertices.Count-2;
		
		vertices.Add(vertices[firstTriangleLow]);
		vertices.Add(vertices[firstTriangleHigh]);
		vertices.Add(vertices[secTriangleLow]);
		vertices.Add(vertices[secTriangleHigh]);
		
		for(int i = 0; i < 4; i++){
			normals.Add(new Vector3(-1,1,0));
		}
		
		triangles.Add(vertices.Count-4);
		triangles.Add(vertices.Count-3);
		triangles.Add(vertices.Count-1);
		
		triangles.Add(vertices.Count-4);
		triangles.Add(vertices.Count-1);
		triangles.Add(vertices.Count-2);
	}

	void MakeDownTriangle(Vector3 pos, float size){
		int insertedVertsIndex = vertices.Count;
		
		vertices.Add( new Vector3(pos.x, pos.y, pos.z));
		vertices.Add( new Vector3(pos.x, pos.y + size, pos.z));
		vertices.Add( new Vector3(pos.x + size, pos.y, pos.z));
		
		triangles.Add( insertedVertsIndex );
		triangles.Add( insertedVertsIndex+1);
		triangles.Add( insertedVertsIndex+2);

		for(int i = 0; i < 3; i++){
			normals.Add(new Vector3( 0, 0, -1));
		}
	}

	void MakePillar(Vector3 pos, float blockSize, float height){
		if(height % blockSize == 0){
			int divisions = (int) (height / blockSize);
			for(int i = 0; i < divisions; i++){
				MakeCube(new Vector3(pos.x, pos.y+(i*blockSize), pos.z),blockSize);
			}
		} else {
			Debug.LogError("MakePillar : height must be a multiple of blockSize");
		}
	}

	void MakeUpPillar(Vector3 pos, float blockSize, float height){
		if(height % blockSize == 0){
			int divisions = (int) (height / blockSize);
			for(int i = 0; i < divisions; i++){
				MakeCube(new Vector3(pos.x, pos.y+(i*blockSize), pos.z),blockSize);
			}

			MakeUpTrapeze(new Vector3(pos.x, pos.y + (divisions*blockSize), pos.z), blockSize);
		} else {
			Debug.LogError("MakePillar : height must be a multiple of blockSize");
		}

		MakeUpTrapeze(new Vector3(pos.x, pos.y + (height*blockSize), pos.z), blockSize);
	}

	void MakeDownPillar(Vector3 pos, float blockSize, float height){
		if(height % blockSize == 0){
			int divisions = (int) (height / blockSize);
			for(int i = 0; i < divisions; i++){
				MakeCube(new Vector3(pos.x, pos.y+(i*blockSize), pos.z),blockSize);
			}
			
			MakeDownTrapeze(new Vector3(pos.x, pos.y + (divisions*blockSize), pos.z), blockSize);
		} else {
			Debug.LogError("MakePillar : height must be a multiple of blockSize");
		}
		
		MakeUpTrapeze(new Vector3(pos.x, pos.y + (height*blockSize), pos.z), blockSize);
	}

	/*void MakeUpSection(Vector3 pos, int height, int width){
		for(int i = 0; i < width; i++){
			MakeUpPillar(new Vector3(pos.x + (i*blockSize), pos.y, pos.z), height + i);
		}
	}*/

	/*void MakeFlatSection(Vector3 pos, int height, int width){
		for(int i = 0; i < width; i++){
			MakePillar(new Vector3(pos.x + (i*blockSize), pos.y, pos.z), height);
		}
	}*/

	/*void MakeDownSection(Vector3 pos, int height, int width){
		for(int i = 0; i < width; i++){
			MakeDownPillar(new Vector3(pos.x + (i*blockSize), pos.y, pos.z), height - i);
		}
	}*/

	void UpdateMesh(){
		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.normals = normals.ToArray();

		mesh.RecalculateBounds();
		mesh.RecalculateNormals();

		meshFilter.mesh = mesh;
	}

}
