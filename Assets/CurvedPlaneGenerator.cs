﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CurvedPlaneGenerator : MonoBehaviour {

	private MeshFilter meshFilter;
	private MeshCollider meshCollider;
	private Mesh mesh;

	private List<Vector3> vertices;
	private List<int> triangles;
	private List<Vector3> normals;

	private List<Vector2> wholeCurve;

	public float step = 1f;
	public float width = 20f;
	public float height = 1f;

	private delegate float CurveFunction(float x, params float[] args);

	// Use this for initialization
	void Start () {
		vertices = new List<Vector3>();
		triangles = new List<int>();
		normals = new List<Vector3>();
		wholeCurve = new List<Vector2>();

		meshFilter = gameObject.GetComponent<MeshFilter>();
		meshCollider = gameObject.GetComponent<MeshCollider>();
		mesh = meshFilter.mesh;

		List<Vector2> testCurve1 = new List<Vector2>{ new Vector2(0f,0f), new Vector2(10f, 10f), new Vector2(20f,10f), new Vector2(30f,20f), new Vector2(40f, 20f), new Vector2(50f, 30f), new Vector2(60f, 30f), new Vector2(70f, 40f), new Vector2(80f, 40f), new Vector2(90f, 50f), new Vector2(100f, 50f)  };
		AddCurveToMesh(testCurve1, height, width, new Vector2(0f, 50f));

		List<Vector2> firstCurve = GenerateCurve(-80f, 100.0f, step, AgnesiCurve, 30f, 1f);
		foreach(Vector2 v in firstCurve){
			wholeCurve.Add(v);
		}
		AddCurveToMesh(firstCurve, height, width);

		List<Vector2> secCurve = GenerateCurve(100.0f, 400.0f, step, SinCurve, 5f, 10f, 1f, 0f);
		foreach(Vector2 sv in secCurve){
			wholeCurve.Add(sv);
		}
		AddCurveToMesh(secCurve, height, width, new Vector2(-150f, 3.5f));

		List<Vector2> thirdCurve = GenerateCurve(200.0f, 300.0f, step, SinCurve, 5f, 10f, 1f, 218.33f);
		foreach(Vector2 tv in thirdCurve){
			wholeCurve.Add(tv);
		}
		//AddCurveToMesh(thirdCurve, 1f, 20f, true);
		/*
		List<Vector2> fourthCurve = GenerateCurve(601.0f, 800.0f, step, AgnesiCurve, 20f, 1f);
		foreach(Vector2 fv in fourthCurve){
			wholeCurve.Add(fv);
		}*/

		Debug.Log("Whole curve has " + wholeCurve.Count + " points");

		UpdateMesh();
	}

	List<Vector2> GenerateCurve(float startValue, float finalValue, float step, CurveFunction curveFunction, params float[] fctArgs){
		List<Vector2> curve = new List<Vector2>();
		float curvedSectionWidth = finalValue - startValue;



		Debug.Log("Generating curve for function " + curveFunction.Method + ", width = " + curvedSectionWidth + ", step = " + step);

		/* Then generate FUNCTION CURVE */
		float currentCurvedLength;
		for(currentCurvedLength = startValue; currentCurvedLength < startValue+curvedSectionWidth; currentCurvedLength += step){
			float y = curveFunction(currentCurvedLength,fctArgs);
			curve.Add(new Vector2( currentCurvedLength, y ));
		}

		Debug.Log("Curve has " + curve.Count + " points, length: " + currentCurvedLength );
		return curve;
	
	}

	float SinCurve(float x, params float[] args){
		float a = args[0];
		float b = args[1];
		float multiplier = args[2];
		float offset = args[3];

		float y = a * Mathf.Sin( x / b );
		y *= multiplier;
		y += offset;

		Debug.Log("SinCurve:  a = " + a + ", b = " + b + ", x = <color=red>" + x + "</color>, y = <color=green>" + y +"</color> multiplier = " + multiplier + " offset = " + offset);
		return y;
	}

	float AgnesiCurve(float x, params float[] args){
		float a = args[0];
		float multiplier = args[1];

		float y = Mathf.Pow(a,3) / ( Mathf.Pow(x,2) + Mathf.Pow(a,2) );
		y *= multiplier;

		Debug.Log("AgnesiCurve:  a = " + a + ", x = <color=red>" + x + "</color>, multiplier = " + multiplier + ", y = <color=green>" + y +"</color>");
		return y;
	}

	void AddCurveToMesh(List<Vector2> curve, float height, float width, Vector2 offset = default(Vector2)){
		curve.Reverse();

		Debug.Log("AddCurveToMesh: curve has " + curve.Count + " mesh had " + vertices.Count + " vertices ");

		/* BOTTOM */
		int firstInsertIndex = vertices.Count;
		foreach(Vector2 v in curve){
			vertices.Add(new Vector3(v.x + offset.x, v.y + offset.y - height/2f, -width/2f));
			vertices.Add(new Vector3(v.x + offset.x, v.y + offset.y - height/2f, width/2f));
		}

		// Iterate vertices 2 at a time to make bottom quads */
		for(int i = firstInsertIndex; i < firstInsertIndex+curve.Count; i += 2){
			triangles.Add( i );
			triangles.Add( i+1 );
			triangles.Add( i+3 );

			triangles.Add( i );
			triangles.Add( i+3 );
			triangles.Add( i+2 );
		}

		for(int v = 0; v < curve.Count*2; v++){
			normals.Add( new Vector3(0,-1,0));
		}

		/* TOP */
		firstInsertIndex = vertices.Count;
		foreach(Vector2 v in curve){
			vertices.Add(new Vector3(v.x + offset.x, v.y + offset.y + height/2f, width/2f));
			vertices.Add(new Vector3(v.x + offset.x, v.y + offset.y + height/2f, -width/2f));
		}
		
		// Iterate vertices 2 at a time to make top quads 
		for(int i = firstInsertIndex; i < firstInsertIndex+curve.Count; i += 2){
			triangles.Add( i );
			triangles.Add( i+1 );
			triangles.Add( i+3 );
			
			triangles.Add( i );
			triangles.Add( i+3 );
			triangles.Add( i+2 );
		}
		
		for(int v = 0; v < curve.Count*2; v++){
			normals.Add( new Vector3(0,1,0));
		}
	}
	

	void UpdateMesh(){
		mesh.vertices = vertices.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.normals = normals.ToArray();


		Debug.Log("Mesh has now :\n <color=red>"+ vertices.Count + "</color> vertices" + "\n <color=red>"+ triangles.Count + "</color> triangles" + "\n <color=red>"+ normals.Count + "</color> normals");
		
		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
		
		meshFilter.mesh = mesh;
		meshCollider.sharedMesh = mesh;
	}
}
